<?php

class RM_DummyModule_Block_Dummymodule extends Mage_Core_Block_Template
{

    public function getDummymoduleCollection()
    {
        $newsCollection = Mage::getModel('rm_dummymodule/dummyModule')->getCollection();
        $newsCollection->setOrder('created', 'DESC');
        return $newsCollection;
    }

}