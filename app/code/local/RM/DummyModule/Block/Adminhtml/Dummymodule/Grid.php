<?php

class RM_DummyModule_Block_Adminhtml_Dummymodule_Grid extends Mage_Adminhtml_Block_Widget_Grid
{

    protected function _prepareCollection()
    {
        $collection = Mage::getModel('rm_dummymodule/DummyModule')->getCollection();
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }

    protected function _prepareColumns()
    {

        $helper = Mage::helper('rm_dummymodule');

        $this->addColumn('id', array(
            'header' => $helper->__('id'),
            'index' => 'id'
        ));

        $this->addColumn('name', array(
            'header' => $helper->__('Name'),
            'index' => 'name',
            'type' => 'text',
        ));
        $this->addColumn('second_name', array(
            'header' => $helper->__('Second_name'),
            'index' => 'second_name',
            'type' => 'text',
        ));
        $this->addColumn('email', array(
            'header' => $helper->__('Email'),
            'index' => 'email',
            'type' => 'email',
        ));

        $this->addColumn('message', array(
            'header' => $helper->__('Message'),
            'index' => 'message',
            'type' => 'text',
        ));

        return parent::_prepareColumns();
    }

    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('dummymodule');

        $this->getMassactionBlock()->addItem('delete', array(
            'label' => $this->__('Delete'),
            'url' => $this->getUrl('*/*/massDelete'),
        ));
        return $this;
    }

    public function getRowUrl($model)
    {
        return $this->getUrl('*/*/edit', array(
            'id' => $model->getId(),
        ));
    }

}
