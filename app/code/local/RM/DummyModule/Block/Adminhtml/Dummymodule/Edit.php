<?php

class RM_DummyModule_Block_Adminhtml_Dummymodule_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{

    protected function _construct()
    {
        $this->_blockGroup = 'rm_dummymodule';
        $this->_controller = 'adminhtml_dummymodule';
    }

    public function getHeaderText()
    {
        $helper = Mage::helper('rm_dummymodule');
        $model = Mage::registry('current_dummymodule');

        if ($model->getId()) {
            return $helper->__("Edit Messages item '%s'", $this->escapeHtml($model->getName()));
        } else {
            return $helper->__("Add Messages item");
        }
    }

}