<?php

class RM_DummyModule_Block_Adminhtml_Dummymodule extends Mage_Adminhtml_Block_Widget_Grid_Container
{

    protected function _construct()
    {
        parent::_construct();

        $helper = Mage::helper('rm_dummymodule');
        $this->_blockGroup = 'rm_dummymodule';
        $this->_controller = 'adminhtml_dummymodule';

        $this->_headerText = $helper->__('Messages Management');
        $this->_addButtonLabel = $helper->__('Add Message');
    }

}