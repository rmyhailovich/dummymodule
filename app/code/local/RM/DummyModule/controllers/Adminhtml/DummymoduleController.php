<?php

class RM_DummyModule_Adminhtml_DummymoduleController extends Mage_Adminhtml_Controller_Action
{


    public function indexAction()
    {
        $this->loadLayout()->_setActiveMenu('rm_dummymodule');
        $this->_addContent($this->getLayout()->createBlock('rm_dummymodule/adminhtml_dummymodule'));
        $this->renderLayout();
    }

    public function newAction()
    {
        $this->_forward('edit');
    }
    
    public function editAction()
    {
        $id = (int) $this->getRequest()->getParam('id');
        Mage::register('current_dummymodule', Mage::getModel('rm_dummymodule/dummyModule')->load($id));

        $this->loadLayout()->_setActiveMenu('rm_dummymodule');
        $this->_addContent($this->getLayout()->createBlock('rm_dummymodule/adminhtml_dummymodule_edit'));
        $this->renderLayout();
    }

    public function saveAction()
    {
        if ($data = $this->getRequest()->getPost()) {
            try {
                $model = Mage::getModel('rm_dummymodule/dummyModule');
                $model->setData($data)->setId($this->getRequest()->getParam('id'));
                $model->save();
                /**
                 * send mail
                 */
                try{
                    $templateData = array('name' => $model->getName()
                    , 'second_name' => $model->getSecondName()
                    , 'message' => $model->getMessage()
                    );

                    Mage::getModel('core/email_template')
                        ->setDesignConfig(array('area'=>'frontend'))
                        ->sendTransactional(
                            'rm_dummymodule_template',
                            'general',
                            Mage::helper("rm_dummymodule")->getEmail(),
                            Null,
                            $templateData);
                }catch (Exception $e){
                    throw new Exception();
                }


                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('Users was saved successfully'));
                Mage::getSingleton('adminhtml/session')->setFormData(false);
                $this->_redirect('*/*/');
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setFormData($data);
                $this->_redirect('*/*/edit', array(
                    'id' => $this->getRequest()->getParam('id')
                ));
            }
            return;
        }
        Mage::getSingleton('adminhtml/session')->addError($this->__('Unable to find item to save'));
        $this->_redirect('*/*/');
    }
    
    
    public function deleteAction()
    {
        if ($id = $this->getRequest()->getParam('id')) {
            try {
                Mage::getModel('rm_dummymodule/dummyModule')->setId($id)->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess($this->__('News was deleted successfully'));
            } catch (Exception $e) {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $id));
            }
        }
        $this->_redirect('*/*/');
    }
    
    
    public function massDeleteAction()
    {
        $dummymodule = $this->getRequest()->getParam('dummymodule', null);

        if (is_array($dummymodule) && sizeof($dummymodule) > 0) {
            try {
                foreach ($dummymodule as $id) {
                    Mage::getModel('rm_dummymodule/dummyModule')->setId($id)->delete();
                }
                $this->_getSession()->addSuccess($this->__('Total of %d messages have been deleted', sizeof($dummymodule)));
            } catch (Exception $e) {
                $this->_getSession()->addError($e->getMessage());
            }
        } else {
            $this->_getSession()->addError($this->__('Please select news'));
        }
        $this->_redirect('*/*');
    }

}