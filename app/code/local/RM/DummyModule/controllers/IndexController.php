<?php

class RM_DummyModule_IndexController extends Mage_Core_Controller_Front_Action
{

    public function indexAction()
    {
        $this->loadLayout();
        $this->_initLayoutMessages('customer/session');
        $this->renderLayout();

    }

    public function newAction()
    {
        $post = $this->getRequest()->getPost();
        if ( $post ) {
            $translate = Mage::getSingleton('core/translate');
            /* @var $translate Mage_Core_Model_Translate */
            $translate->setTranslateInline(false);
            $first_name = (string) $this->getRequest()->getPost('first_name');
            $second_name = (string) $this->getRequest()->getPost('second_name');
            $email = (string) $this->getRequest()->getPost('email');
            $massage = (string) $this->getRequest()->getPost('message');
            
            try {
                $postObject = new Varien_Object();
                $postObject->setData($post);

                $error = false;

                if (!Zend_Validate::is(count(trim($post['first_name'])) , 'NotEmpty')) {
                    $error = true;
                    Mage::getSingleton('customer/session')->addError(Mage::helper('rm_dummymodule')->__('Please complete the field First Name'));
                }

                if (mb_strlen($post['first_name']) <= 2 ) {
                    $error = true;
                    Mage::getSingleton('customer/session')->addError(Mage::helper('rm_dummymodule')->__('Field First Name must be at least 2 characters.'));
                    Zend_Debug::dump($post);
                    Zend_Debug::dump($post['first_name']);
                }

                if (!Zend_Validate::is(trim($post['second_name']) , 'NotEmpty')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['email']), 'EmailAddress')) {
                    $error = true;
                }

                if (!Zend_Validate::is(trim($post['message']), 'NotEmpty')) {
                    $error = true;
                }

                if ($error) {
                    throw new Exception();
                }

                $translate->setTranslateInline(true);
                Mage::getModel('rm_dummymodule/DummyModule')->saveContact($first_name, $second_name, $email, $massage);
                Mage::getSingleton('customer/session')->addSuccess(Mage::helper('rm_dummymodule')->__('Your inquiry was submitted and will be responded to as soon as possible. Thank you for contacting us.'));
                $this->_redirect('*/*/');

                return;
            } catch (Exception $e) {
                $translate->setTranslateInline(true);

                $this->_redirect('*/*/');
                return;
            }

        } else {
            $this->_redirect('*/*/');
        }
    }

}