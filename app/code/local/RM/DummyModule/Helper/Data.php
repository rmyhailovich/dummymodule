<?php

class RM_DummyModule_Helper_Data extends Mage_Core_Helper_Abstract{
    public function getEmail(){
        return Mage::getStoreConfig('rm_dummymodule/rm_dummymodule/email');
    }
}