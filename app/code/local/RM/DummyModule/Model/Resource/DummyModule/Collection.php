<?php

class RM_DummyModule_Model_Resource_DummyModule_Collection extends Mage_Core_Model_Mysql4_Collection_Abstract
{

    public function _construct()
    {
        parent::_construct();
        $this->_init('rm_dummymodule/dummyModule');
    }

}