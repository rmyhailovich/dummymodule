<?php

class RM_DummyModule_Model_DummyModule extends Mage_Core_Model_Abstract{

    protected function _construct(){
        
        $this->_init('rm_dummymodule/DummyModule');
    }

    public function saveContact($firstName, $secondName, $email, $massage ){

        $model = Mage::getModel("rm_dummymodule/DummyModule");
        $model->setName($firstName);
        $model->setSecondName($secondName);
        $model->setEmail($email);
        $model->setMessage($massage);
        Zend_Debug::dump($model->debug());
        $model->save();
    }
}

