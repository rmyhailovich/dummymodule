<?php


$installer = $this;

$tableNews = $installer->getTable('rm_dummymodule/rm_dummymodule');

$installer->startSetup();

$installer->getConnection()->dropTable($tableNews);

$table = $installer->getConnection()
    ->newTable($tableNews)
    ->addColumn('id', Varien_Db_Ddl_Table::TYPE_INTEGER, null, array(
        'identity'  => true,
        'nullable'  => false,
        'primary'   => true,
    ))
    ->addColumn('name', Varien_Db_Ddl_Table::TYPE_TEXT, '60', array(
        'nullable'  => false,
    ))
    ->addColumn('second_name', Varien_Db_Ddl_Table::TYPE_TEXT, '60', array(
        'nullable'  => false,
    ))
    ->addColumn('email', Varien_Db_Ddl_Table::TYPE_TEXT, 150, array(
        'nullable'  => true,
        'default'   => null,
    ))
    ->addColumn('message', Varien_Db_Ddl_Table::TYPE_TEXT, null, array(
        'nullable'  => false,
    ))

    ->addIndex('idx_name', 'name')
    ->addIndex('idx_second_name', 'second_name')
    ->addIndex('idx_email', 'email');

$installer->getConnection()->createTable($table);

$installer->endSetup();

